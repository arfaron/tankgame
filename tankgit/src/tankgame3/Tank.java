/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tankgame3;

/**
 *
 * @author Faris
 */
class Tank {

	// horizontal coordinate value for tank
	int x = 0;
	int color;
	boolean isLive = true;
	int direct = 1;
	int speed = 2;

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	// 0 represents up, 1=right, 2=down, 3=left

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getDirect() {
		return direct;
	}

	public void setDirect(int direct) {
		this.direct = direct;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	// vertical coordinate value
	int y = 0;

	public Tank(int x, int y) {
		this.x = x;
		this.y = y;
	}
}