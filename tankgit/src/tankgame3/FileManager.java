package tankgame3;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.swing.filechooser.FileSystemView;

public class FileManager {
    
    public static final Path folderPath = Paths.get(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\TankGame\\");
    public static final Path filePath = Paths.get(folderPath.toString() + "\\save.txt");
    
    public boolean getFileExistence() {
        return filePath != null;
    }
    
    public ArrayList<String> load() throws IOException {
        ArrayList<String> lines = new ArrayList<String>();
        
        lines = (ArrayList<String>) Files.readAllLines(filePath);
        
        return lines;
    }
    
    public void save(int level, int lastScore) throws IOException {
        new File(folderPath.toString()).mkdirs();
        
        ArrayList<String> lines = new ArrayList<String>();
        
        File f = new File(filePath.toString());
        
        if (f.exists()) {
            lines = (ArrayList<String>) Files.readAllLines(filePath);
            lines.removeAll(lines);
        }
        
        lines.add((level+1) + "");
        lines.add(lastScore + "");
        
        Files.write(filePath, lines, Charset.forName("UTF-8"));
    }
}
